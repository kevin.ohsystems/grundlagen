# Lehrmittel zu den Grundlagen der Digitalisierung

Idee zu einem aufklärerischen Buch / Lehrmittel (inkl. Online-Kurs) zu den Grundlagen des Internets: Wie Netzwerke, Server, Verschlüsselung, Programmieren etc funktioniert, einfach erklärt und vernetzt mit den gesellschaftlich - politischen Frage- und Problemstellungen der jeweiligen Themen. Dabei geht es um Sachen wie "Warum wir das Internet verloren haben" wie um die Geschäftsmodelle von Google, Apple, Microsoft, Amazon, wem gehören die Kabel, warum ist es ein Problem wenn es nur zwei Anbieter/-innen von Betriebssystemen für Handys gibt etc.

Kevin Mueller, Dezember 2021
